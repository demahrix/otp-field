import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:sms_autofill/sms_autofill.dart';


class OtpField extends StatefulWidget {

  /// Nombre de champ
  final int count;
  final List<String?>? initialValue;
  final double fieldWidth;
  final double spaceWidth;
  final TextInputType? keyboardType;
  final InputDecoration? decoration;
  final TextStyle? textStyle;
  final TextAlign textAlign;

  /// Quand on remplie tous les champs
  final ValueChanged<String>? onCompleted;

  /// Est appele lorsqu'on que la valeur d'un champ change
  /// Sauf si tout les champs sont remplie et que `onCompleted` est definie
  final ValueChanged<List<String?>>? onChanged;

  const OtpField({
    Key? key,
    required this.count,
    this.initialValue,
    this.fieldWidth = 24.0,
    this.spaceWidth = 16.0,
    this.keyboardType = TextInputType.number,
    this.decoration,
    this.textAlign = TextAlign.center,
    this.textStyle = const TextStyle(
      fontSize: 24.0,
      fontWeight: FontWeight.w700
    ),
    this.onCompleted,
    this.onChanged
  }): super(key: key);
  /// TODO  assert(initialValue?.length == count),

  @override
  _OtpFieldState createState() => _OtpFieldState();

}

///_ _ _ _ _
///012345678
class _OtpFieldState extends State<OtpField> with CodeAutoFill {

  late final List<FocusNode> _focusNodes;
  late final List<TextEditingController> _controllers;
  late final List<String?> _values;

  @override
  void initState() {
    super.initState();
    listenForCode();

    final int length = widget.count;
    _values = List<String?>.generate(length, (index) => widget.initialValue?[index]);
    _focusNodes = List<FocusNode>.generate(length, (index) => FocusNode(onKeyEvent: (_, key) => _onKey(index, _, key)), growable: false);
    _controllers = List<TextEditingController>.generate(length, (index) => TextEditingController(text: _values[index]), growable: false);

    SmsAutoFill().getAppSignature.then((value) {
      log(value, name: "APP SIGNATURE");
    });

  }

  @override
  void codeUpdated() {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(code?.toLowerCase() ?? 'N/A')));
    log(code?.toLowerCase() ?? 'N/A', name: 'CODE RECEIVED');
  }

  /// Lorsqu'on appuie sur effacer dans un champ déjà vide la methode [_onChanged] ne reagit pas à cet évènement
  KeyEventResult _onKey(int index, FocusNode _, KeyEvent event) {
    if (event is KeyDownEvent && event.logicalKey == LogicalKeyboardKey.backspace && _values[index] == null && index != 0) {
      _focusNodes[index - 1].requestFocus();
      return KeyEventResult.handled;
    }
    return KeyEventResult.ignored;
  }

  void _onChanged(int index, String value) {

    if (value.isEmpty) {
      // On vient de supprimer un element
      _values[index] = null;
      widget.onChanged?.call(_values.toList());
      return;
    }

    // On vient d'entrer une valeur

    int len = value.length;

    String? newValue;

    if (len == 1) { 
      // Lorsqu'on vient d'entrer un caractere dans un champ auparavant vide
      newValue = value;
    } else {
      // Lorsqu'on vient d'entrer un caractere dans un champ qui contient deja un caractere

      value = value.trim();

      final TextEditingValue textEditingValue = value.isEmpty
        ? TextEditingValue.empty
        : TextEditingValue(text: value[value.length - 1], selection: const TextSelection(baseOffset: 1, extentOffset: 1));

      _controllers[index].value = textEditingValue;

      newValue = value.isEmpty ? null : value[value.length - 1];
    }

    _values[index] = newValue;

    if (newValue != null) {

      if (index == widget.count - 1) { // le dernier champ
        _focusNodes[index].unfocus();
      } else if (index < widget.count) { // les autres champs
        _focusNodes[index + 1].requestFocus();
      }
    }

    if (widget.onCompleted != null && _isCompleted()) {
      widget.onCompleted!(_values.join());
    } else {
      widget.onChanged?.call(_values.toList());
    }
  }

  bool _isCompleted() {
    for (int i=0, len=_values.length; i<len; ++i)
      if (_values[i] == null)
        return false;
    return true;
  }

  @override
  Widget build(BuildContext context) {

    int length = widget.count;

    return Row(
      mainAxisSize: MainAxisSize.min,
      children: List.generate(length * 2 - 1, (index) {

        if ((index & 1) == 1)
          return SizedBox(width: widget.spaceWidth);

        final int realIndex = index ~/ 2;

        return SizedBox(
          width: widget.fieldWidth,
          child: TextField(
            controller: _controllers[realIndex],
            focusNode: _focusNodes[realIndex],
            keyboardType: widget.keyboardType,
            decoration: widget.decoration ?? const InputDecoration(),
            style: widget.textStyle,
            textAlign: widget.textAlign,
            textAlignVertical: TextAlignVertical.center,
            onChanged: (value) => _onChanged(realIndex, value),
          ),
        );
      }),
    );
  }

  @override
  void dispose() {
    for (int i=0; i<widget.count; ++i) {
      _focusNodes[i].dispose();
      _controllers[i].dispose();
    }

    super.dispose();
    cancel();
  }

}
