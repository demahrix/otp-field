<!-- 
This README describes the package. If you publish this package to pub.dev,
this README's contents appear on the landing page for your package.

For information about how to write a good package README, see the guide for
[writing package pages](https://dart.dev/guides/libraries/writing-package-pages). 

For general information about developing packages, see the Dart guide for
[creating packages](https://dart.dev/guides/libraries/create-library-packages)
and the Flutter guide for
[developing packages and plugins](https://flutter.dev/developing-packages). 
-->


<!-- ![Screenshot 1](exemple/img1.png)
![Screenshot 2](exemple/img2.png)
![Screenshot 3](exemple/img3.png) -->

<img src="exemple/img1.png" width="135">
<img src="exemple/img2.png" width="135">
<img src="exemple/img3.png" width="135">

TODO: Put a short description of the package here that helps potential users
know whether this package might be useful for them.

# TODO ajouter les exemples

## Features

TODO: List what your package can do. Maybe include images, gifs, or videos.

## Getting started

TODO: List prerequisites and provide or point to information on how to
start using the package.

## Usage

TODO: Include short and useful examples for package users. Add longer examples
to `/example` folder. 

```dart
import 'package:flutter/material.dart';
import 'package:otp_field/otp_field.dart';

class Exemple extends StatelessWidget {
  const Exemple({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: Center(
        child: OtpField(
          count: 5,
          keyboardType: TextInputType.phone,
        ),
      ),
    );
  }
}
```

## Additional information

TODO: Tell users more about the package: where to find more information, how to 
contribute to the package, how to file issues, what response they can expect 
from the package authors, and more.
